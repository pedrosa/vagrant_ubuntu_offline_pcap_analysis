# Provision VM for Offline PCAP Analysis
***

## Information
This project creates a Ubuntu Precise 32bits virtual machine on VirtualBox, with tools and scripts for off-line analysis of captured network packets.

## Dependencies
This project requires having [VirtualBox][vb] and [Vagrant][vg] installed.

## How to use it

Create the machine:

* Clone this project
* Open vagrant/boot.sh and attribute your OINKCODE to the variable. Request OINKCODE on [snort site][snort]
* Go to the directory of the project/vagrant and run command:

        vagrant up

* Use the following command to connect to the machine.

        vagrant ssh

* Don't forget to change, in snort.conf:
 
        ipvar HOME_NET any

      to (or to your home network)

        ipvar HOME_NET [10.0.0.0/8,172.16.0.0/12,192.168.0.0/16]  
 
* Also, activate emerging rules on /etc/snort/rules/emerging.conf. Attention: BLOCK RULES are not installed.

Use it:

* You can share the files from the host machine to the virtual machine by using the directory *data/* on the host machine and access them on the */vagrant_data* directory on the virtual machine.
* On the virtual machine go to to the directory that has the PCAPs and execute:

        offline_pcap_nsm_analysis.sh file.pcap

The result of the analysis can be found in the user home under directory *NSM_Analysis*.

Good analysis.

## Credits
The script *offline_pcap_nsm_analysis.sh* is original from <http://nillepill.blogspot.pt/2007_05_01_archive.html>



[snort]: https://www.snort.org/account/oinkcode "Get Oinkcode"
[vb]: https://www.virtualbox.org/wiki/Downloads "VirtualBox"
[vg]: http://www.vagrantup.com/downloads.html  "Vagrant"

***
## Todos
1. Optimize the provision script, to verify if some downloads and installation's are already done, to move to the next steps.
2. Sometimes downloading snort rules  fails, failing all the installation process on the first execution …

