#!/bin/bash -   
#title          :boot.sh
#description    :Vagrant provision for offline nsm pcap analisys
#author         :Tiago Pedrosa pedrosa.tiago[@]gmail.com
#date           :20140124
#version        :0.1    
#usage          :On vagrant request to use provision using shell boot.sh
#notes          :       
#bash_version   :3.2.48(1)-release
#============================================================================


#Configuration variables
OINKCODE=

#System update
echo "System Update"
sudo apt-get update
sudo apt-get -y upgrade
echo "---------------"
#-----------------------------

#Install prerequisites for compiling Snort.
echo "Install prerequisites for compiling Snort, and others"
apt-get install -y flex bison build-essential checkinstall libpcap-dev libnet1-dev libpcre3-dev libmysqlclient15-dev libnetfilter-queue-dev iptables-dev gcc make libpcap0.8-dev unzip
echo "---------------"
#-----------------------------

#Next, build and install libdnet The "-fPIC" C flag is necessary if you compile it on 64-bit platform.
echo "Complile libdnet "
cd /usr/local/src/
wget --no-check-certificate https://libdnet.googlecode.com/files/libdnet-1.12.tgz
tar xvfvz libdnet-1.12.tgz
cd libdnet-1.12
./configure "CFLAGS=-fPIC"
make
make install
ln -s /usr/local/lib/libdnet.1.0.1 /usr/lib/libdnet.1 
echo "---------------"
#-----------------------------

#Install DAQ
echo "Complile DAQ"
cd /usr/local/src
wget --no-check-certificate http://www.snort.org/downloads/2657 -O daq-2.0.1.tar.gz
tar xfvz daq-2.0.1.tar.gz
cd daq-2.0.1/
./configure
make
make install
echo "---------------"
#-----------------------------

#Install snort
echo "Compile Snort"
cd /usr/local/src
wget --no-check-certificate http://www.snort.org/downloads/2665 -O snort-2.9.5.6.tar.gz 
tar xfvz snort-2.9.5.6.tar.gz 
cd snort-2.9.5.6
./configure
make
make install
echo "---------------"
#-----------------------------

#Reload libraries
echo "Reload libraries"
ldconfig -v
echo "---------------"
#-----------------------------

#Download snort rule set
echo "Download snort rule set. Check OINKCODE"
cd /usr/local/src
mkdir /etc/snort
wget --no-check-certificate https://www.snort.org/reg-rules/snortrules-snapshot-2956.tar.gz/$OINKCODE -O snortrules-snapshot-2956.tar.gz 
tar xfvz snortrules-snapshot-2956.tar.gz -C /etc/snort
touch /etc/snort/rules/white_list.rules /etc/snort/rules/black_list.rules
mkdir /usr/local/lib/snort_dynamicrules


#emerging threads
echo "Downloading emergingthreats rules"
cd /usr/local/src
wget http://rules.emergingthreats.net/open/snort-2.9.0/emerging.rules.tar.gz -O emerging.rules.tar.gz

tar xfvz emerging.rules.tar.gz -C /etc/snort/

echo "include \$RULE_PATH/emerging.conf" >> /etc/snort/etc/snort.conf
chown -R vagrant:vagrant /etc/snort/*
echo "---------------"
#-----------------------------

echo "Teste snort conf"
sudo snort -T -i eth0 -c /etc/snort/etc/snort.conf
echo "---------------"
#-----------------------------

# Install tcpdstat -> http://t3chkommie.com/game-review/tcpdstat 
echo "Install tcpdStat"
cd /usr/local/src
wget https://github.com/netik/tcpdstat/archive/master.zip
unzip master.zip
cd tcpdstat-master
make
make install	
echo "---------------"
#-----------------------------


#install wireshark, tshark, argus-client, argus-server,
echo "Install wireshark, tshark, argus-server, argus-client ...."
apt-get install -y wireshark tshark argus-client argus-server p0f
echo "---------------"
#-----------------------------

#Post install scriptss
echo "Clean source files"
rm -rf /usr/local/src/*
echo "---------------"
#-----------------------------

#Linkar script
echo "Linking offline_pcap_nsm_analisys.sh"
ln -s /vagrant_data/offline_pcap_nsm_analisys.sh /bin/
echo "---------------"
#-----------------------------

echo " hange # ipvar HOME_NET any \n
ipvar HOME_NET [10.0.0.0/8,172.16.0.0/12,192.168.0.0/16] "

echo "Activate emerging rules on /etc/snort/rules/emerging.conf . BLOCK RULES not installed"
