#!/bin/sh
#-------
# Changed for use on ubuntu precise. Vagrant recipe with provising available. Tiago Pedrosa pedrosa.tiago[at]gmail.com
# Downloaded from: http://nillepill.blogspot.pt/2007/05/offline-pcap-nsm-analysis-script.html . Original Credites and Header follows
# - - -
# Offline pcap NSM Analysis Script - Tested on Mr Bejtlich FreeBSD 5.4 VM ( www.SGUIL.net )
# I made this script for personal use primarily, nice if you find it useful.
# nillepill[at]gmail.com
# TODO: Fix working Tethereal options in script variable
# - - -
# Would like to recommend TaoSecurity Training, http://www.taosecurity.com/training.html
# I really enjoyed attending the 4 day Network Security Operations Course
# - - -

# Description
# Takes input from specified tcpdump file
# Creates new folders in users home directory, puts all analysis output in those directorys
#

# Enviroment
WORK_DIR=$HOME
FILE=$1
CASE_FOLDER=$(date +%H-%M-%S)-$1
ANALYSIS_FOLDER=NSM_Analysis
DATE_FOLDER=$(date +%Y-%m-%d)
HASH_DIR=HASHES
# Tools
DATE=/bin/date

#install tcpdstat http://t3chkommie.com/game/revirew/tcpdstat
TCPDSTAT=/usr/local/bin/tcpdstat
CAPINFOS=/usr/bin/capinfos
TCPDUMP=/usr/sbin/tcpdump

#Now is tshark
TETHEREAL=/usr/bin/tshark
TETHEREAL_OPTIONS="-qz io,phs -nr"
P0F=/usr/sbin/p0f
SNORT=/usr/local/bin/snort
SNORT_CONF=/etc/snort/etc/snort.conf
ARGUS=/usr/sbin/argus
RAGATOR=/usr/bin/ragator
RACOUNT=/usr/bin/racount
RAHOSTS=/usr/bin/rahosts
RA=/usr/bin/ra
MD5=/usr/bin/md5sum
SHA1=/usr/bin/sha1sum
# Usage description
if [ -z "$1" ]; then
echo ""
echo "-----------------------------------"
echo "Offline pcap NSM Analysis Script"
echo "-----------------------------------"
echo ""
echo Usage: $0 interface.pcap
echo ""
exit
fi

# Start Analysis
echo ""
echo "Starting Offline pcap NSM Analysis Script"
$DATE
echo ""

echo "# Create folders, copy file and set to read only"
mkdir $WORK_DIR/$ANALYSIS_FOLDER
mkdir $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER
mkdir $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER
mkdir $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$HASH_DIR
cp $1 $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/
chmod 400 $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$1
echo "# Create hashes"
$MD5 $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$1 > $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$HASH_DIR/$1.sha1
$SHA1 $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$1 > $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$HASH_DIR/$1.md5
echo "# Set hash mod"
chmod 400 $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$HASH_DIR/$1.sha1
chmod 400 $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$HASH_DIR/$1.md5
echo "# Set dir mod"
chmod 700 $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$HASH_DIR/
echo "# Run statistical tools"
$CAPINFOS $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE > $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.txt.capinfos
$TCPDSTAT $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE > $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.txt.tcpdstat

#$TETEHEREAL $TETHEREAL_OPTIONS $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE > $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.txt.tethereal 
tshark -qz io,phs -nr $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE > $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.txt.tethereal 
echo "# Run Session tools"
$ARGUS -r $FILE -w $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.arg.bin
$RAGATOR -r $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.arg.bin -w $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.arg.bin.ragator
$RACOUNT -ar $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.arg.bin.ragator > $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.arg.txt.racount
$RAHOSTS -nr $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.arg.bin.ragator > $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.arg.txt.rahosts
$RA -nn -r $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.arg.bin.ragator -s saddr daddr dport proto | sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4 | uniq -c > $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.arg.txt.ra.session
echo "# Fingerprint identifiable hosts"
$P0F -U -s $FILE -o $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.txt.p0f
echo "# Run Alert tools"
$SNORT -c $SNORT_CONF -b -y -r $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE -l $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/
echo "# Copy hostinfo to case"
uname -a >> $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.txt.analysis.host
date >> $WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/$FILE.txt.analysis.date
echo "# Finish!"
echo ""
echo "------------------------------------------------------"
$DATE
echo "Analysis finished, data are ready for review in"
echo "$WORK_DIR/$ANALYSIS_FOLDER/$DATE_FOLDER/$CASE_FOLDER/"
echo ""
echo "------------------------------------------------------"
echo ""
